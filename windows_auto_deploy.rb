require 'aws-sdk'
require 'devops/aws'
require 'base64'
require 'openssl'

@awsRegion   = ARGV[0] #'ap-northeast-1'
@TargetArn   = ARGV[1] #"arn:aws:elasticloadbalancing:ap-northeast-1:962462911341:targetgroup/credit-TA-GROUP/bb52c6c378c784ff"
key          = ARGV[2] #'/Users/changhanwen/Desktop/awskey/Staging-ECFE.pem'
pscode       = ARGV[3]

# start from here
ec2_client    = Aws::EC2::Client.new(region: @awsRegion)
elbv2_client  = Aws::ElasticLoadBalancingV2::Client.new(region: @awsRegion)
dev           = Devops::Aws.new

# For Each EC2 instance on ALB Target Group
array = dev.elbv2_instances(elbv2_client, @TargetArn)
array.each do |index|
  # Get IP for each instance
  executeOn = dev.get_ec2_private_ip(ec2_client, index['id'])



  # dev.elbv2_deregister_targets(elbv2_client, index['id'], @TargetArn)
  # dev.wait_target_deregistered(elbv2_client, @TargetArn, index['id'])

  if @TargetArn.downcase.index('crmapi') && key.downcase.index('prod')
    system("python ./deploy.py '#{executeOn}' '!qaz2wsx' '#{pscode}'")
  else
    # Decrypted Windows Password
    encrypted_pswd = ec2_client.get_password_data({instance_id: index['id']})
    private_key = OpenSSL::PKey::RSA.new(File.read(File.expand_path(key)))
    decrypted_pswd = private_key.private_decrypt(Base64.decode64(encrypted_pswd.password_data))

    system("python ./deploy.py '#{executeOn}' '#{decrypted_pswd}' '#{pscode}'")
  end

  # dev.elbv2_register_targets(elbv2_client, index['id'], @TargetArn)
  # dev.wait_target_in_service(elbv2_client, @TargetArn, index['id'])
end
